const _ = require('./boxed');

_.defn('sum', [_.INT, _.INT], _.INT, (a, b) => a + b);
_.defn('sum', [_.FLT, _.INT], _.FLT, (a, b) => a - b);
console.log(_.call('sum', [1.2, 5]));
