const fns = {};

const types = {
    NIL: 'N',
    INT: 'Z',
    FLT: 'R',
    STR: 'S',
};

const validators = {
    [types.NIL]: (x) => {
        return x === null || x === undefined;
    },
    [types.INT]: (x) => {
        return typeof(x) === 'number' && Number.isInteger(x);
    },
    [types.FLT]: (x) => {
        return typeof(x) === 'number';
    },
    [types.STR]: (x) => {
        return typeof(x) === 'string';
    },
}

module.exports = {
    defn(name, params, ret, body) {
        if (!(name in fns)) {
            fns[name] = new Map();
        }
        const signature = params.join('');
        if (signature in fns) {
            throw Error(`Function with signature ${name}(${signature}) already defined`);
        }
        fns[name][signature] = { ret, body };
    },
    call(name, args) {
        if (!(name in fns)) {
            throw Error(`Function ${name} not defined`);
        }
        const methods = fns[name];
        for(const methodSignature in methods) {
            const validatorKeys = methodSignature.split('');
            if (args.length === validatorKeys.length) {
                let match = true;
                for(let i=0; i<args.length; i++) {
                    match = match && validators[validatorKeys[i]](args[i]);
                    if (!match) {
                        break;
                    }
                }
                if (match) {
                    const method = methods[methodSignature];
                    const result = method.body(...args);
                    if (!validators[method.ret](result)) {
                        throw Error(`Returned value ${result} doesn't satisfy return signature ${method.ret}`);
                    }
                    return result;
                }    
            }
        }
        throw Error(`None of the defined methods matched provided arguments`);
    },
    ...types
};
